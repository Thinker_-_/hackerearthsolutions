import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;

public class SyncTest {

	Object lock1=new Object();
	Object lock2=new Object();
	Object lock3=new Object();
	
	static Map<String,AtomicLong> map=new HashMap<String,AtomicLong>();
	static Map<String,AtomicLong> orders=Collections.synchronizedMap(map);
	
	public void A(){
		synchronized (lock1) {
				System.out.println(Thread.currentThread().getName()+" called A");
			System.out.println("Sleeping");
			try {
				Thread.sleep(6000);
				System.out.println(Thread.currentThread().getName()+" end");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void B(){
		synchronized (lock2) {
			System.out.println(Thread.currentThread().getName()+" called B");
			System.out.println("Sleeping");
			try {
				Thread.sleep(6000);
				System.out.println(Thread.currentThread().getName()+" end");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public void C(){
		synchronized (lock3){
			
			System.out.println(Thread.currentThread().getName()+" called C");
			System.out.println("Sleeping");
			try {
				Thread.sleep(6000);
				System.out.println(Thread.currentThread().getName()+" end");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
/*	public static void main(String args[]){
		SyncTest test=new SyncTest();
		Runnable r=new Runnable() {
			
			@Override
			public void run() {
				test.A();
	Runnable r1=new Runnable() {
			}
		};
			
			@Override
			public void run() {
				test.B();
			}
		};
	Runnable r2=new Runnable() {
			
			@Override
			public void run() {
				test.C();
			}
		};
		
		Thread t[]=new Thread[8];
		
		t[0]=new Thread(r,"Thread0");
		t[1]=new Thread(r1,"Thread1");
		t[2]=new Thread(r2,"Thread2");
		t[3]=new Thread(r1,"Thread3");
		t[4]=new Thread(r2,"Thread4");
		t[5]=new Thread(r1,"Thread5");
		t[6]=new Thread(r2,"Thread6");
		t[7]=new Thread(r1,"Thread7");
		System.out.println("main theread waiting");
		for(int i=0;i<t.length;i++){
			t[i].start();
		}
		
		for(int i=0;i<t.length;i++){
			try {
				t[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("main theread waiting end");
		
		
	}*/
	
	static void processOrders(){
		for(String  city: orders.keySet()){
			for(int i=0;i<50;i++){
				orders.get(city).getAndIncrement();
			}
		}
	}
	//https://www.hackerrank.com/challenges/longest-increasing-subsequent/problem
	
	static int output[];
	static boolean[] isProccessed;
	public static int longestIncreasingSubsequence(int input[]){
		output=new int[input.length];
		isProccessed=new boolean[input.length];
		isProccessed[input.length-1]=true;
		output[input.length-1]=1;
		int maxSoFar=0;
		
		for(int i=0;i<input.length;i++){
			maxSoFar=Math.max(maxSoFar, SS(i,input));
		}
		return maxSoFar;
	}
	public static int SS(int index,int input[]){
		if(isProccessed[index])
			   return output[index];
		
		int maxSoFar=0;
		for(int i=index+1;i<input.length;i++){
			if(input[i]>input[index])
				maxSoFar=Math.max(maxSoFar, SS(i,input));
		}
		output[index]=maxSoFar+1;
		isProccessed[index]=true;
		return output[index];
	}
	public static void main(String args[]){
	  try {
		FileInputStream input=new FileInputStream(new File("/home/prashank/Documents/a.java"));
		Scanner sc=new Scanner(input);
		try {
			Thread.sleep(10000);
			sc.nextLine();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	}
	
	static class id{
		String dob;
		int id;
		
		public id(String dob,int id){
			this.id=id;
			this.dob=dob;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((dob == null) ? 0 : dob.hashCode());
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			id other = (id) obj;
			if (dob == null) {
				if (other.dob != null)
					return false;
			} else if (!dob.equals(other.dob))
				return false;
			if (id != other.id)
				return false;
			return true;
		}
		
	}
}
