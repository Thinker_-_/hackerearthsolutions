package com.graph.api;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;
 
/**
 * A Generic graph API for implementing graph related operations.
 * It is an open source API is some wants to add some methods related
 * graphs, then they are welcome to do that.
 * @author Prashank
 *
 * @param <T>
 */
public class JGraph {
		
	private Map<Integer,Vertex> verticies;
	
	private boolean isDirected;
	
	public Map<Integer, Vertex> getVerticies() {
		return verticies;
	}
 
	public void setVerticies(Map<Integer, Vertex> verticies) {
		this.verticies = verticies;
	}
 
	public JGraph(boolean isDirected){
		this.verticies=new LinkedHashMap<Integer,Vertex>();
		this.isDirected=isDirected;
	}
	
	public int getSize() {
		return verticies.size();
	}
 
 
	public boolean isDirected() {
		return isDirected;
	}
 
	public void setDirected(boolean isDirected) {
		this.isDirected = isDirected;
	}
 
    public void addEdge(Integer source,Integer destination,long weight){
		Vertex sourceVertex=this.verticies.get(source);
		if(sourceVertex==null){
			sourceVertex=new Vertex(source);
			this.verticies.put(source, sourceVertex);
		}
		Vertex destinationVertex=this.verticies.get(destination);
		
		if(destinationVertex==null){
			destinationVertex=new Vertex(destination);
			this.verticies.put(destination, destinationVertex);
		}
		sourceVertex.addEdge(destinationVertex, weight);
		if (!isDirected) {
			destinationVertex.addEdge(sourceVertex, weight);
		}
	}
	
	public void printEdges(){
		Set<Entry<Integer, Vertex>> verticies=this.verticies.entrySet();
		Iterator<Entry<Integer, Vertex>> itr=verticies.iterator();
		while(itr.hasNext()){
			Entry<Integer, Vertex> entry=itr.next();
			Vertex vertex=entry.getValue();
			for(Edge edge:vertex.adjacencylist){
				System.out.println(edge);
			}
		}
	}
	public static void main(String args[]) throws IOException{
		Scanner sc=null;
		BufferedReader br=null;
	   try {
		sc=new Scanner(new BufferedInputStream(new FileInputStream("/home/prashank/Desktop/input.txt"),2048));
	} catch (Exception e1) {
		e1.printStackTrace();
	}
		int vertices=sc.nextInt();
		long edges=sc.nextLong();
		long expectedCost=sc.nextLong();
		JGraph graph=new JGraph(false);
		
		Comparator<Vertex> com=new Comparator<JGraph.Vertex>() {
 
			@Override
			public int compare(Vertex o1, Vertex o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		};
		PriorityQueue<Vertex> minHeap=new PriorityQueue<Vertex>(vertices,com);
		PriorityQueue<Vertex> maxHeap=new PriorityQueue<Vertex>(vertices,com.reversed());
		for(int i=1;i<=vertices;i++){
			Vertex v=graph.new Vertex(i);
			if(i==1){
				v.key=0L;
				v.parent=-1;
			}else{
				v.key=1000000000000L;
				v.parent=null;
			}
			graph.verticies.put(i, v);
			minHeap.add(v);
		}
		long start=System.currentTimeMillis();
		for(int i=0;i<edges;i++){
			int source=sc.nextInt();
			int des=sc.nextInt();
			int cost=sc.nextInt();
		graph.addEdge(source, des, cost);
		}
		boolean removed[]=new boolean[vertices];
		long minimumCost=0L;
		while(!minHeap.isEmpty()){
			Vertex v=minHeap.poll();
			if(v.getParent()!=null){
				minimumCost+=v.getKey();
			}else{
				System.out.println(-1);
				return ;
			}
			removed[v.value-1]=true;
			for(Edge e:v.adjacencylist){
				Vertex desitation=e.getDestination();
				if(!removed[desitation.getValue()-1] && desitation.getKey()>e.getWeight()){
					minHeap.remove(desitation);
					desitation.setKey(e.getWeight());
					desitation.setParent(v.getValue());
					minHeap.add(desitation);
				}
			}
			maxHeap.add(v);
		}
 
		int superCoolRoad=0;
		while(!maxHeap.isEmpty() && superCoolRoad<vertices-1){
			if(minimumCost>expectedCost){
				Vertex v=maxHeap.poll();
				minimumCost=minimumCost-v.getKey()+1;
				superCoolRoad+=1;
			}else
				break;
 
		}
		if(minimumCost<=expectedCost)
		System.out.println(superCoolRoad);
		else
			System.out.println(-1);
	}
	
	
	public class Edge{
		Vertex source;
		Vertex destination;
		long weight;
	
		public Vertex getSource() {
			return source;
		}
 
		public Vertex getDestination() {
			return destination;
		}
 
		public void setDestination(Vertex destination) {
			this.destination = destination;
		}
 
		public long getWeight() {
			return weight;
		}
 
		public void setWeight(long weight) {
			this.weight = weight;
		}
 
		public void setSource(Vertex source) {
			this.source = source;
		}
 
 
		public Edge(){}
		
		public Edge(Vertex source,Vertex destination,long weight){
			this.source=source;
			this.destination=destination;
			this.weight=weight;
		}
		
		public String toString(){
			return this.getSource()+"----"+this.getWeight()+"---->"+this.getDestination();
		}
	}
	
	
	public class Vertex{
		Integer value;
		
		List<Edge> adjacencylist;
		
		Integer parent;
		
		Long key;
		
		public Integer getParent() {
			return parent;
		}
		public void setParent(Integer parent) {
			this.parent = parent;
		}
		public Long getKey() {
			return key;
		}
		public void setKey(Long key) {
			this.key = key;
		}
		public Integer getValue() {
			return value;
		}
		public void setValue(Integer value) {
			this.value = value;
		}
		public List<Edge> getAdjacencylist() {
			return adjacencylist;
		}
		public void setAdjacencylist(List<Edge> adjacencylist) {
			this.adjacencylist = adjacencylist;
		}
		public Vertex(){}
		public Vertex(Integer value){
			this.value=value;
			this.adjacencylist=new LinkedList<Edge>();
		}
		
		public void addEdge(Vertex destination,long weight){
			this.adjacencylist.add(new Edge(this, destination, weight));
		}
		public String toString(){
			return this.value.toString();
		}
	}
	
}