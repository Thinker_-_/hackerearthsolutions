package com.hackerearth.problems;

import java.io.BufferedInputStream;
import java.util.Scanner;
import static java.lang.Integer.parseInt;
public class BitPattern {

	/**
	 * m and n will not be zero in the test cases
	 * as per the input guide lines
	 * @param inputString
	 * @param m
	 * @param n
	 * @return
	 */
	public static boolean checkPosiblity(String inputString,int m,int n){
		
		int k=0;
		int l=0;
		char inputCharArray[]=inputString.toCharArray();
		for(char c:inputCharArray){
			if(c=='0')
				k++;
			else
				l++;
		}
		int c1=k/m;
		int c2=l/n;
		if(k%m!=0 || l%n!=0 || c2>c1 || c1>c2+1 || k==0 || l==0)
			return false;

		return true;
	}
	
	public static void main(String args[]){
		Scanner sc=new Scanner(new BufferedInputStream(System.in));
		int t=parseInt(sc.nextLine());
		while(t-->0){
			String [] input=sc.nextLine().split(" ");
			int m=parseInt(input[0]);
			int n=parseInt(input[1]);
			input=sc.nextLine().split(" ");
			int length=parseInt(input[0]);
			System.out.println(checkPosiblity(sc.nextLine(), m, n)?"Yes":"No");
		}

		sc.close();
	}

}
