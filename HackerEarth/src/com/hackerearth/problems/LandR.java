package com.hackerearth.problems;

import java.io.BufferedInputStream;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class LandR {
	
	private static TreeMap<Integer,Integer> BST=new TreeMap<Integer,Integer>();
	
		
	public static void main(String args[]){
		Scanner sc=new Scanner(new BufferedInputStream(System.in));
		int n=sc.nextInt();
		int p[]=new int[n];
		for(int i=0;i<n;i++){
			p[i]=sc.nextInt();
		}
		Arrays.sort(p);
		for(int i=0;i<n;i++)
			BST.put(p[i],i);
		int Q=sc.nextInt();
		while(Q-->0){
			Entry<Integer,Integer> i=BST.ceilingEntry(sc.nextInt());
			if(i==null){
				System.out.println(0);
			    continue;
			}
	//		System.out.println(i);
			Entry<Integer,Integer> j=BST.floorEntry(sc.nextInt());
		//	System.out.println(j);
			System.out.println(j.getValue().intValue()-i.getValue().intValue()+1);
			
		}
		sc.close();
	}
	
}
