package com.hackerearth.graphs;

import java.util.function.BinaryOperator;

/**
 * @author Prashank Jauhari.
 * @param <T>
 */
public class JSegmentTree<T extends Comparable> implements BinaryOperator<T>{

    final private  node<T> root;

    final BinaryOperator<T> operator;

    final T sentinal;

    public JSegmentTree(final BinaryOperator<T> operator,final T [] data,final T sentinal){
            if(operator==null)
                this.operator=this;
            else
                this.operator=operator;

            this.sentinal=sentinal;
            this.root=new node<>(0,data.length-1,null);
            init(data);
    }

    public JSegmentTree(final T [] data,final T sentinal){
            this.operator=this;
            this.sentinal=sentinal;
            this.root=new node<>(0,data.length-1,null);
            init(data);
    }

    private void init(T data[]){
        createTree(this.root,data);
    }

    private   void createTree(node<T> root,T data[]){
         if(root.getI()==root.getJ()) {
            root.setData(data[root.getI()]);
         }
         else {
             int mid=getMid(root.getI(),root.getJ());
             node<T> left=new node(root.getI(),mid,null);
             createTree(left,data);
             node<T> right=new node(mid+1,root.getJ(),null);
             createTree(right,data);
             root.setLeft(left);
             root.setRight(right);
             root.setData(operator.apply(left.getData(),right.getData()));
         }

    }

   public T getOperationUtilties(int left,int right){
        return getOperation(left,right,root);
   }

   public void updateUtilities(int index,T value){
        update(index,value,root);
   }

   private T getOperation(int left,int right,node<T> root){

        if(root.getI() >= left &&  root.getJ() <=right)
              return root.getData();

        else if(right < root.getI() || left > root.getJ()  ){
            return sentinal;

        }else{
           return operator.apply(getOperation(left,right,root.getLeft()),
                   getOperation(left,right,root.getRight()));
        }
    }

    private void update(int i,T value,node<T> root){
        if(root.getI()<=i && root.getJ()>=i){
            if(root.getI()==i && root.getJ()==i){
                root.setData(value);
            }else {
                update(i, value, root.getLeft());
                update(i, value, root.getRight());
                root.setData(operator.apply(root.getLeft().getData(), root.getRight().getData()));
            }
        }
    }


    private final  static  int getMid(int i,int j){
        return  (int)Math.floor((double)(i+j)/2);
    }


    /**
     * Default is for min range
     * based query.
     * @param t
     * @param t2
     * @return
     */
    @Override
    public T apply(T t, T t2) {
       if(t.compareTo(t2)==-1)
            return t;
       else
           return t2;
    }

   private static class node<T>{

        private int i;
        private int j;
        private T data;
        private node<T> left;
        private node<T> right;

        public node(int i,int j,T data){
            this.i=i;
            this.j=j;
            this.data=data;
        }

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        public int getJ() {
            return j;
        }

        public void setJ(int j) {
            this.j = j;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public node<T> getLeft() {
            return left;
        }

        public void setLeft(node left) {
            this.left = left;
        }

        public node<T> getRight() {
            return right;
        }

        public void setRight(node right) {
            this.right = right;
        }
    }
}

