import java.io.PrintWriter;

public class AVLTrees {

    node root;

    public void preorder(node root, StringBuffer writer){
        if(root==null)
             return;
        writer.append(root.data+",");
        preorder(root.left,writer);
         preorder(root.right,writer);
    }
    public node rightRotate(node y){
        node arg0=y.parent;
        node z=y.left;
        node w=z.right;
        z.right=y;
        y.parent=z;
        y.left=w;
        if(w!=null)
           w.parent=y;

        y.height=1+Math.max(height(y.left),height(y.right));
        z.height=1+Math.max(height(z.left),height(z.right));
        z.parent=arg0;
        if(arg0!=null) {
            if (arg0.data > z.data)
                arg0.left = z;
            else
                arg0.right = z;
        }
        return z;
    }

    private node leftRotate(node z){
        node arg0=z.parent;
        node y=z.right;
        node w=y.left;
        y.left=z;
        z.parent=y;
        z.right=w;
        if(w!=null)
          w.parent=z;

        z.height=1+Math.max(height(z.left),height(z.right));
        y.height=1+Math.max(height(y.left),height(y.right));
        y.parent=arg0;
        if(arg0!=null) {
            if (arg0.data > y.data)
                arg0.left = y;
            else
                arg0.right = y;
        }
        return y;
    }

    public void insert(int data){
        if(root==null){
            root=new node(null,null,null,1,data);
            return;
        }
        //normal insert in bst starts
        node y=findLocationToInsert(data);

        if(y==null)
            return;
        node w=new node(null,null,y,1,data);
        if(data<y.data) {
            y.left = w;
        }
        else {
            y.right = w;
        }
        //normal insert in bst end w is the reference of newly inserted node
        y.height=1+Math.max(height(y.left),height(y.right));

        node z=y.parent;

        while(z!=null){
            int bf=getBalance(height(z.left),height(z.right));
            //that mean elements is inserted on left side as bf>1 and it is LL im-balance
            if(bf>1 && data<z.left.data){
              z= rightRotate(z);
            }
            //LR imbalance
            if(bf>1 && data>z.left.data){
                z.left= leftRotate(z.left);
            z= rightRotate(z);
            }
            //RR im-balance
            if(bf<-1 && data>z.right.data){
               z=leftRotate(z);
            }

            //RL imbalnce
            if(bf<-1 && data<z.right.data){
                z.right=rightRotate(z.right);
                z=leftRotate(z);
            }
            z.height=1+Math.max(height(z.left),height(z.right));

            if(z.parent==null) {
                root = z;
            }
            z=z.parent;

        }
    }

    private static int getBalance(int leftHeight,int rightHeight){
        return leftHeight-rightHeight;
    }
    private static int height(node n){
       return n!=null?n.height:0;
    }

    private node findLocationToInsert(int data){
        node arg1=null;
        node arg0=root;
        while(arg0!=null){
            arg1=arg0;
            if(data==arg0.data)
                return null;

            if(data<arg0.data)
                arg0=arg0.left;
            else
                arg0=arg0.right;

        }

        return arg1;
    }

    class node{
        node left;
        node right;
        node parent;
        int data;
        int height=1;

        private node(node left,node right,node parent,int height,int data){
            this.left=left;
            this.right=right;
            this.parent=parent;
            this.height=height;
            this.data=data;
        }
    }

}
