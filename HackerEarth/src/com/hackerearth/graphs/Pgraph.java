package com.hackerearth.graphs;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

import com.graph.api.JGraph.Edge;

public class Pgraph {
	
	int verticies;
	int edges;
	vertex[] verticiesArray;
	
	public Pgraph(int verticies,int edges){
		this.verticies=verticies;
		this.edges=edges;
		verticiesArray=new vertex[verticies];
		for(int i=1;i<=verticies;i++)
			verticiesArray[i]=new vertex(i);
	}
	
	public void addEdge(int source,int destination,long weight){
		Edge e=new Edge(source, destination, weight);
		vertex sourcevertex=this.verticiesArray[source];
		vertex destinationvertex=this.verticiesArray[destination];
		sourcevertex.getAdjcencyList().add(e);
		Edge e2=new Edge(destination,source, weight);
		destinationvertex.getAdjcencyList().add(e2);
	}
	
	
	class Edge{
		vertex source;
		vertex destination;
		long weight;
		public Edge(int source,int destination,long weight){
			this.source=verticiesArray[source];
			this.destination=verticiesArray[destination];
			this.weight=weight;
		}
	}
	
	class vertex{
		
		int value;

		LinkedList<Edge> adjcencyList;
		
		public vertex(int value){
		 this.value=value;
		 adjcencyList=new LinkedList<Edge>();
		}
		
		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
		}

		public LinkedList<Edge> getAdjcencyList() {
			return adjcencyList;
		}

		public void setAdjcencyList(LinkedList<Edge> adjcencyList) {
			this.adjcencyList = adjcencyList;
		}

		
	}
	
	public static void main(String args[]){
		Scanner sc=null;	
		try {
			//sc=new Scanner(new BufferedInputStream(new FileInputStream("/home/prashank/Desktop/input.txt"),2048));
			sc=new Scanner(new BufferedInputStream(System.in));
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			sc.close();
		}

	}

}
