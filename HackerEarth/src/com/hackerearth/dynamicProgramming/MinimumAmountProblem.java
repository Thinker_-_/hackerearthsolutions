
package com.hackerearth.dynamicProgramming;
import java.io.BufferedInputStream;
import java.util.Scanner;

/**
 * This problem can only be solved by either 
 * exaustive search not by greedy.
 * As there is optimal substructure and
 * repitative problem then dynamic problem
 * time complexity O(sum*totalFruits)
 * can be of some help.
 * 
 * space Complexity O(sum*totalFruit)
 * @author Prashank Jauhari
 */
public class MinimumAmountProblem {

	static int sum;
	static int count1;
	static int count2;
	static int count3;
	static int cost1;
	static int cost2;
	static int cost3;
	
	static void printMinimumExpanses(int [][] table,int row,int col){
		for(int i=0;i<row;i++){
			  for(int j=0;j<col;j++){
					System.out.print(String.format("%d ", table[i][j]));
			  }
			  System.out.println();
		}
	}
	
	static void printPossiblity(boolean [][] posiblity,int row,int col){
		for(int i=0;i<row;i++){
			  for(int j=0;j<col;j++){
					System.out.print(String.format("%b ", posiblity[i][j]));
				}
				System.out.println();
			
		}
	}
	static int getMinimumExpanse(){
		sum=sum+1;
		int count=count1+count2+count3+1;
		int table[][]=new int[count][sum];
		boolean posiblity[][]=new boolean[count][sum];
		table[0][0]=0;
		posiblity[0][0]=true;
		
		for(int i=1;i<sum;i++){
			table[0][i]=0;
		}
		for(int i=1;i<count;i++){
			table[i][0]=0;
			posiblity[i][0]=true;
		}
//		System.out.println("Init Done");
/*	System.out.println("Initial Expanses \n");
		printMinimumExpanses(table, count, sum);
		System.out.println("Possiblity \n");
		printPossiblity(posiblity, count, sum);*/
		for(int fruit=1;fruit<count;fruit++){
			for(int currentSum=1;currentSum<sum;currentSum++){
				int cost=0;
				boolean possible=false;
				int reducedSum=currentSum-getEnergy(fruit);
				if(reducedSum<0)
					 reducedSum=0;
				if(posiblity[fruit-1][reducedSum]){
					cost=getCost(fruit)+table[fruit-1][reducedSum];
					possible=true;
				}
				if(possible && posiblity[fruit-1][currentSum]){
					cost=Math.min(cost, table[fruit-1][currentSum]);
				}else if(posiblity[fruit-1][currentSum]){
					cost=table[fruit-1][currentSum];
				}
				table[fruit][currentSum]=cost;
				posiblity[fruit][currentSum]=possible;
			}
		}
		
/*		System.out.println("Final Expanses \n");
		printMinimumExpanses(table, count, sum);
		System.out.println("Possiblity \n");
		printPossiblity(posiblity, count, sum);*/
		if(posiblity[count-1][sum-1]){
			return table[count-1][sum-1];
		}else
			return -1;
	}

	static int getCost(int index){
	  
		if(index== 0)
			return 0;
		
		if(index<=count1)
			return cost1;
		else if(index >count1 && index <=count2+count1)
			return cost2;
		else
			return cost3;
	}
	static int getEnergy(int index){
		if(index==0)
			return 0;
		if(index<=count1)
			return 2;
		else if(index >count1 && index <=count2+count1)
			return 3;
		else
			return 4;		
	}
	public static void main(String args[]){
		Scanner sc=new Scanner(new BufferedInputStream(System.in));
		sum=sc.nextInt();
		count1=sc.nextInt();
		count2=sc.nextInt();
		count3=sc.nextInt();
		cost1=sc.nextInt();
		cost2=sc.nextInt();
		cost3=sc.nextInt();
		System.out.println(getMinimumExpanse());
		sc.close();
	}
	
}
