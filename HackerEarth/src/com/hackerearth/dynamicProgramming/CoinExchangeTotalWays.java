package com.hackerearth.dynamicProgramming;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class CoinExchangeTotalWays {

    static  int d[];

    static int total[][];

    static int total(int i,int sum) {
         if(sum==0)
            return 1;
        else if(sum<0)
            return 0;
        else if(total[i][sum]!=-1)
            return total[i][sum];

        else {
            int t=0;
            for (int k=i; k < d.length; k++)
                t+=total(k,sum-d[k]);


            CoinExchangeTotalWays.total[i][sum]=t;

            return t;
        }

    }

static void init(int sum){
      total=  new int[d.length][sum+1];

  for(int i=0;i<d.length;i++){
      for(int j=0;j<=sum;j++){
          if(j==0)
              total[i][0]=1;
          else
              total[i][j]=-1;

      }
  }
}
    public static void main(String args[]){

        try(Scanner sc=new Scanner(new BufferedInputStream(System.in));){
            int sum=sc.nextInt();

            int dCount=sc.nextInt();
            d=new int[dCount];

            for(int i=0;i<d.length;i++)
                d[i]=sc.nextInt();
            init(sum);
        System.out.println(total(0,sum));
        }
    }
}
