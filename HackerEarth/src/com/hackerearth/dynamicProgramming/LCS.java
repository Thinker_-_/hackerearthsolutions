package com.hackerearth.dynamicProgramming;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.util.Scanner;

public class LCS {

	static int table[][];
	static char solution[][];
	static int n[];
	static int m[];
	static void findLCS(){
		for(int i=0;i<=n.length;i++){
			for(int j=0;j<=m.length;j++){
				if(i==0 || j==0){
					table[i][j]=0;
					if(i==0 && j==0)
						solution[i][j]='/';
					else if(i==0 && j!=0)
						solution[i][j]='L';
					else
						solution[i][j]='U';
				}
				else if(n[i-1]!=m[j-1]){
					if(table[i-1][j]> table[i][j-1]){
						solution[i][j]='U';
					}else{
						solution[i][j]='L';
					}
					table[i][j]=Math.max(table[i-1][j], table[i][j-1]);
				}else{
					solution[i][j]='*';
					table[i][j]=1+table[i-1][j-1];
				}
			}
		}
	}
	static int[] getLCS(){ 	
		int i=n.length;
		int j=m.length;
		int lcs[]=new int[table[i][j]];
		int k=table[i][j];
		while(i > 0 && j>0){
			if(solution[i][j]=='*'){
				lcs[k-1]=n[i-1];
				k=k-1;
				i=i-1;
				j=j-1;
			}else if(solution[i][j]=='U'){
				i=i-1;
			}else{
				j=j-1;
			}
		}
		return lcs;
	}
	
	static void printSolution(){
		for(int i=0;i<=n.length;i++){
			for(int j=0;j<=m.length;j++){
				System.out.print(String.format("%d ", table[i][j]));
			}
			System.out.println();
		}
	}

	public static void main(String arg[]){
		Scanner sc=null;
		try{
			sc=new Scanner(new BufferedInputStream(System.in));
			int n=sc.nextInt();
			
			
			int m=sc.nextInt();
		
			table=new int[n+1][m+1];
			solution=new char[n+1][m+1];
			
			LCS.n=new int[n];
			for(int i=0;i<n;i++)
				LCS.n[i]=sc.nextInt();
			
			LCS.m=new int[m];
			
			for(int j=0;j<m;j++)
				LCS.m[j]=sc.nextInt();
			
			LCS.findLCS();
			System.out.println("START");
			LCS.printSolution();
			System.out.println("END");
			int lcs[]=LCS.getLCS();			
//			System.out.println(LCS.printLCS());
			//System.out.println(table[n][m]);
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			sc.close();
		}

	}
}
