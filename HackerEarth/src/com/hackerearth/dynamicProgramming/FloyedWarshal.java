package com.hackerearth.dynamicProgramming;

import java.io.BufferedInputStream;
import java.util.Scanner;

public class FloyedWarshal {

	public static long [][] calculateAllPairShortestPath(long graph[][],int row,int col){
		long graph2[][]=new long[row][col];
		boolean flag=true;
		
		for(int i=0;i<row;i++){
			for(int j=0;j<row;j++){
				for(int k=0;k<col;k++){
					if(flag){
					graph2[j][k]=Math.min(graph[j][k], graph[j][i]+graph[i][k]);	
					}else{
					graph[j][k]=Math.min(graph2[j][k], graph2[j][i]+graph2[i][k]);	
					}
				}
			}
			flag = !flag;
		}
		if(flag)
			return graph;
		else
			return graph2;
	}
	public static long calculateMinimumDistance(long [][] shortestpathGraph,int shrines,int rows){

		int visitedShrines=0;
		long minimumDistance=0L;
		long distance[]=new long[shrines];
		for(int i=0;i<shrines;i++)
			    distance[i]=shortestpathGraph[0][i];
			   
		while(visitedShrines<shrines){
			long min[]=min(distance);
			minimumDistance+=min[0];
			distance[(int)min[1]]=-1;
			for(int i=0;i<shrines;i++)
			    distance[i]=Long.min(distance[i],shortestpathGraph[(int)min[1]][i]);
			visitedShrines++;
		}
		return minimumDistance;
	}
	public static long[] min(long distance[]){
		long [] min=new long[2];
		min[0]=Integer.MAX_VALUE;
		min[1]=-1;
		
		for(int i=0;i<distance.length;i++){
			if(min[0]>distance[i] && distance[i]!=-1){
				min[0]=distance[i];
				min[1]=i;
			}
		}
		return min;
	}
	

	public static long [][] init(int rows,int column){
		long [][] graph=new long[rows][column];
		for(int i=0;i<rows;i++){
			for(int j=0;j<column;j++){
				if(i==j)
					graph[i][j]=0;
				else
					graph[i][j]=Integer.MAX_VALUE;
			}
		}
		return graph;
	}
	
	public static void print(long [][] graph,int rows,int column){
		for(int i=0;i<rows;i++){
			for(int j=0;j<column;j++){
			   System.out.print(new StringBuffer().append(graph[i][j]).append(" "));
			}
			System.out.println();
		}
	}
	public static void main(String args[]){
		Scanner sc=null;
		try{
			sc=new Scanner(new BufferedInputStream(System.in));
			int testcase=sc.nextInt();
			while(testcase>0){
				int cities=sc.nextInt();
				int roads=sc.nextInt();
				int shrines=sc.nextInt();
				long [][]graph=init(cities, cities);
				for(int i=0;i<roads;i++){
					int a=sc.nextInt()-1;
					int b=sc.nextInt()-1;
					int c=sc.nextInt();
					graph[a][b]=c;
					graph[b][a]=c;
				}
				graph=calculateAllPairShortestPath(graph,cities,cities);
				System.out.println(calculateMinimumDistance(graph, shrines,cities));
				testcase--;
			}



		}catch(Exception e){
			e.printStackTrace();
		}finally {
			sc.close();
		}


	}

}
