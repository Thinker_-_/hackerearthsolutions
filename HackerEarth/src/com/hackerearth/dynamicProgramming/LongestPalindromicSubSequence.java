package com.hackerearth.dynamicProgramming;

/**
 * User tabulation approach
 * (Bottom up dynamic programming
 * approach) for solving this problem.
 * Time Complexity O(n^2)
 * Space Complexity O(n^2)
 */
import java.io.BufferedInputStream;
import java.util.Scanner;
import static java.lang.Integer.parseInt;
public class LongestPalindromicSubSequence {
	static int testcases;
	static Scanner sc;
	static int calulateLongestPalidromSubSequenceLength(StringBuffer input){
		int[][] table=new int[input.length()+1][input.length()+1];
		int length=1;
		
		/**
		 * Total number of Iteration will be n^2
		 */
		while(length<=input.length()){
			for(int i=1;i<=input.length();i++){
				for(int j=i+length-1;j-i+1==length && j<=input.length() ;j=j+length){
					if(i==0 || j==0)
						table[i][j]=0;
					else if(i>j)
						table[i][j]=0;
					else if(j-i == 1 && input.charAt(i-1)==input.charAt(j-1))
						table[i][j]=2;
					else if(j-i == 1 && input.charAt(i-1)!=input.charAt(j-1))
						table[i][j]=1;
					else if(i==j)
						table[i][j]=1;
					else{
						if(input.charAt(i-1)!=input.charAt(j-1))
							table[i][j]=Math.max(table[i][j-1], table[i+1][j]);
						else
							table[i][j]=2+table[i+1][j-1];
					}
				}
			}
			length++;
		}
		return table[1][input.length()];
	}
	static void printtable(int table[][],int n,int m){
		for(int i=0;i<n;i++){
			for(int j=0;j<m;j++){
				System.out.print(String.format("%d ", table[i][j]));
			}
			System.out.println();
		}
	}
	public static void main(String args[]){
		try{
			StringBuffer buffer=new StringBuffer("");
			sc=new Scanner(new BufferedInputStream(System.in));
			testcases=parseInt(sc.nextLine());
			while(testcases-->0){
				StringBuffer input=new StringBuffer(sc.nextLine());
				buffer.append(String.format("%d \n", calulateLongestPalidromSubSequenceLength(input)));
			}
			System.out.println(buffer.toString());
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			sc.close();
		}
	}

}
