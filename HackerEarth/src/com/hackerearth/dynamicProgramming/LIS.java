package com.hackerearth.dynamicProgramming;

import java.io.*;
import java.util.*;


public class LIS {
static int output[];
    static boolean[] isProccessed;
    // Complete the longestIncreasingSubsequence function below.
        public static int longestIncreasingSubsequence(int input[]){
        output=new int[input.length];
        isProccessed=new boolean[input.length];
        isProccessed[input.length-1]=true;
        output[input.length-1]=1;
        int maxSoFar=0;
        
        for(int i=0;i<input.length;i++){
            maxSoFar=Math.max(maxSoFar, SS(i,input));
        }
        return maxSoFar;
    }
        public static int SS(int index,int input[]){
        if(isProccessed[index])
               return output[index];
        
        int maxSoFar=0;
        for(int i=index+1;i<input.length;i++){
            if(input[i]>input[index])
                maxSoFar=Math.max(maxSoFar, SS(i,input));
        }
        output[index]=maxSoFar+1;
        isProccessed[index]=true;
        return output[index];
    }

    private static final Scanner scanner = new Scanner(System.in);

    
    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            int arrItem = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
            arr[i] = arrItem;
        }

        int result = longestIncreasingSubsequence(arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}



