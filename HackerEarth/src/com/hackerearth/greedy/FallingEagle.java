package com.hackerearth.greedy;

import static java.lang.Long.parseLong;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import static java.lang.Integer.parseInt;
import java.util.Comparator;
import java.util.PriorityQueue;

public class FallingEagle {
	static class node{
		int a;
		int b;
		int i;
		int j;
		
		public node(int a,int b,int i,int j){
			this.a=a;
			this.b=b;
			this.i=i;
			this.j=j;
		}
		public int getA() {
			return a;
		}
		public void setA(int a) {
			this.a = a;
		}
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
		public int getI() {
			return i;
		}
		public void setI(int i) {
			this.i = i;
		}
		public int getJ() {
			return j;
		}
		public void setJ(int j) {
			this.j = j;
		}
		
	}
	static PriorityQueue<node> buildMinHeap(int n,Comparator<node> com,String input[]){
		PriorityQueue<node> minHeap=new PriorityQueue<node>(n-1,com);
		for(int i=0;i<n-1;i++){
			node pair=new node(parseInt(input[i]),parseInt(input[i+1]),i,i+1);
			minHeap.add(pair);
			}
		return minHeap;
	}
	static long printPair(PriorityQueue<node> minHeap,int n,String input[]){
		boolean del[]=new boolean[n];
		long strength=0L;
		while(!minHeap.isEmpty()){
			node pair=minHeap.poll();
			if(pair!=null && !del[pair.i] && !del[pair.j]){
				if(pair.a<=pair.b){
					del[pair.i]=true;
					strength+=pair.b;
					if(0<pair.i){
						int i=0;
						for(i=pair.i-1;i>=0 && del[i];i--){}
						if(i>=0){
							node nw=new node(parseInt(input[i]),parseInt(input[pair.j]),i,pair.j);
						minHeap.add(nw);
						}
						}
				}
				else{
					del[pair.j]=true;
					strength+=pair.a;
					if(pair.j<n-1){
						int i=0;
						for(i=pair.j+1;i<n && del[i];i++){}
						if(i<n){
							node nw=new node(parseInt(input[pair.i]),parseInt(input[i]),pair.i,i);
							minHeap.add(nw);
						}
						}
				}
			}
				
		}
		return strength;
	}

	public static void main(String args[]){
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Comparator<node> minPQComparator= new Comparator<node>() {
			@Override
			public int compare(node n1,node n2) {
				if((n1.a==n2.a && n1.b==n2.b) || (n1.a==n2.b && n1.b==n2.a)){
					return 0;
				}
				int max1=Math.max(n1.a, n1.b);
				int max2=Math.max(n2.a, n2.b);
				if(max1>max2){
					return 1;
				}
				if(max1<max2)
					return -1;
				int i1=n1.a==max1?n1.a:n1.b;
				int i2=n2.a==max2?n2.a:n2.b;
				if(i1>i2)
					return -1;
				else
					return 1;
			}
		};

			int t=(int)parseLong(br.readLine());
		while(t>0){
			int n=(int)parseLong(br.readLine());
			String input[]=br.readLine().split(" ");
			PriorityQueue<node> minPQ=buildMinHeap(n, minPQComparator,input);
			System.out.println(printPair(minPQ, n,input));
			t--;
		}
		br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
}
}
	